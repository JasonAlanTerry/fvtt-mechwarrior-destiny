// Import Modules
import { MWDestinyActor } from "./module/actor/actor.js";
import { MWDestinyActorSheet } from "./module/actor/actor-sheet.js";
import { MWDestinyItem } from "./module/item/item.js";
import { MWDestinyItemSheet } from "./module/item/item-sheet.js";

Hooks.once('init', async function() {

  game.mwd = {
    MWDestinyActor,
    MWDestinyItem
  };

  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "",
  };

  // Define custom Entity classes
  CONFIG.Actor.entityClass = MWDestinyActor;
  CONFIG.Item.entityClass = MWDestinyItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("mwd", MWDestinyActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("mwd", MWDestinyItemSheet, { makeDefault: true });

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function() {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function(str) {
    return str.toLowerCase();
  });
});